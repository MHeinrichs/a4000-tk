-- Version 1.0 02-18-94
-- Revision List: None
-- Package Type: 18 x 18 PGA
entity MC68060 is
generic(PHYSICAL_PIN_MAP:string := "PGA_18x18");
port (
TDI: in bit;
TDO: out bit;
TMS: in bit;
TCK: in bit;
TRST: in bit;
D: inout bit_vector(0 to 31);
A: inout bit_vector(0 to 31);
CLA: in bit;
TM: out bit_vector(0 to 2);
TLN: out bit_vector(0 to 1);
R_W: out bit;
SIZ: out bit_vector(0 to 1);
LOCKE: out bit;
LOCK: out bit;
BR: out bit;
BB: inout bit;
SNOOP: in bit;
TIP: out bit;
TS: inout bit;
BTT: inout bit;
SAS: out bit;
PST: out bit_vector(0 to 4);
TA: in bit;
TEA: in bit;
TRA: in bit;
BG: in bit;
BGR: in bit;
TBI: in bit;
AVEC: in bit;
TCI: in bit;
CLK: in bit;
CLKEN: in bit;
IPL: in bit_vector(0 to 2);
RSTI: in bit;
CDIS: in bit;
MDIS: in bit;
BS: out bit_vector(0 to 3);
RSTO: out bit;
IPEND: out bit;
CIOUT: out bit;
UPA: out bit_vector(0 to 1);
TT1: inout bit;
TT0: out bit;
JTAGENB: in bit_vector(0 to 2);
THERM1: linkage bit;
THERM0: linkage bit;
EVDD: linkage bit_vector(1 to 25);
EVSS: linkage bit_vector(1 to 25);
IVDD: linkage bit_vector(1 to 16);
IVSS: linkage bit_vector(1 to 16)
);
use STD_1149_1_1994.all;
attribute COMPONENT_CONFORMANCE of MC68060: entity is "STD_1149_1_1993" ;
attribute PIN_MAP of MC68060 : entity is PHYSICAL_PIN_MAP;
-- PGA_18x18 Pin Map
--
-- No-connects: D4, D5, D6, D7, D9, D11, D13, D14, K4, M4, N4, Q16, R7,
-- R10, S12, T9, T12
--
constant PGA_18x18 : PIN_MAP_STRING :=
"TDI: S3, " &
"TDO: T2, " &
"TMS: S5, " &
"TCK: S4, " &
"TRST: T3, " &
-- 0 1 2 3 4 5 6 7 8 9 10 11
"D: ( C3, B3, C4, A2, A3, A4, A5, A6, B7, A7, A8, A9, " &
-- 12 13 14 15 16 17 18 19 20 21 22 23
" A10, A11, A12, A13, B11, A14, B12, A15, A16, A17, B16, C15, " &
-- 24 25 26 27 28 29 30 31
" A18, C16, B18, D16, C18, E16, E17, D18 ), " &
-- 0 1 2 3 4 5 6 7 8 9 10 11
"A: ( L18, K18, J17, J18, H18, G18, G16, F18, E18, F16, P1, N3, " &
-- 12 13 14 15 16 17 18 19 20 21 22 23
" N1, M1, L1, K1, K2, J1, H1, J2, G1, F1, E1, G3, " &
-- 24 25 26 27 28 29 30 31
" D1, F3, E2, C1, E3, B1, D3, A1 ), " &
"CLA: K15, " &
"TM: ( N18, M18, K17 ), " &
"TLN: ( Q18, P18 ), " &
"R_W: N16, " &
"SIZ: ( P17, P16 ), " &
"LOCKE: R18, " &
"LOCK: S18, " &
"BR: T18, " &
"BB: T17, " &
"SNOOP: P15, " &
"TIP: R15, " &
"TS: R16, " &
"BTT: Q15, " &
"SAS: Q14, " &
"PST: ( T15, S14, R14, T16, Q13 ), " &
"TA: T14, " &
"TEA: S13, " &
"TRA: Q12, " &
"BG: T13, " &
"BGR: Q11, " &
"TBI: S11, " &
"AVEC: T11, " &
"TCI: T10, " &
"JTAGENB: ( T4, F15, S10 ), " &
"CLK: R9, " &
"CLKEN: Q8, " &
"IPL: ( T8, T7, T6 ), " &
"RSTI: S7, " &
"CDIS: T5, " &
"MDIS: S6, " &
"BS: ( Q4, Q5, Q6, Q7 ), " &
"RSTO: R3, " &
"IPEND: S1, " &
"CIOUT: R1, " &
"UPA: ( Q3, Q1 ), " &
"TT1: P2, " &
"TT0: P3, " &
"THERM1: M15, " &
"THERM0: L15, " &
"EVDD: ( B5, B9, B14, C2, C17, D8, D10, D12, D15, E4, G2, " &
" G4, G15, G17, J4, J15, L4, M2, M17, N15, P4, Q10, " &
" R2, R17, S16 ), " &
"EVSS: ( B2, B4, B6, B8, B10, B13, B15, B17, D2, D17, F2, " &
" F4, F17, H2, H17, L2, L17, N2, N17, Q2, Q9, Q17, " &
" S2, S15, S17 ), " &
"IVDD: ( C5, C8, C10, C12, C14, E15, H3, H16, J3, J16, L16, " &
" M3, R5, R8, R12, S8 ), " &
"IVSS: ( C6, C7, C9, C11, C13, H4, H15, K3, K16, L3, M16, " &
" R4, R6, R11, R13, S9 ) ";
-- Other Pin Maps here
attribute TAP_SCAN_IN of TDI:signal is true;
attribute TAP_SCAN_OUT of TDO:signal is true;
attribute TAP_SCAN_MODE of TMS:signal is true;
attribute TAP_SCAN_CLOCK of TCK:signal is (33.0e6, BOTH);
attribute TAP_SCAN_RESET of TRST:signal is true;
attribute COMPLIANCE_PATTERNS of MC68060: entity is
"(JTAGENB(0), JTAGENB(1), JTAGENB(2)) (000)";
attribute INSTRUCTION_LENGTH of MC68060:entity is 4;
attribute INSTRUCTION_OPCODE of MC68060:entity is
"EXTEST (0000)," &
"LPSAMPLE (0001)," &
"SAMPLE (0100)," &
"IDCODE (0101)," &
"CLAMP (0110)," &
"HIGHZ (0111)," &
"PRIVATE (0010, 0011, 1000,1001,1010,1011,1100,1101,1110)," &
"BYPASS (1111)";
attribute INSTRUCTION_CAPTURE of MC68060: entity is "0101";
attribute INSTRUCTION_PRIVATE of MC68060:entity is "PRIVATE";
attribute REGISTER_ACCESS of MC68060:entity is
"BOUNDARY (LPSAMPLE)";
attribute IDCODE_REGISTER of MC68060: entity is
"0001" & -- version
"000001" & -- design center
"0000110000" & -- sequence number
"00000001110" & -- Motorola
"1"; -- required by 1149.1
attribute BOUNDARY_LENGTH of MC68060:entity is 214;
attribute BOUNDARY_REGISTER of MC68060:entity is
--num cell port function safe ccell dsval rslt
"0 (BC_1, D(0), input, X), " &
"1 (BC_2, D(0), output3, X, 4, 0, Z), " &
"2 (BC_1, D(1), input, X), " &
"3 (BC_2, D(1), output3, X, 4, 0, Z), " &
"4 (BC_2, *, control, 0), " & -- d[3:0]
"5 (BC_1, D(2), input, X), " &
"6 (BC_2, D(2), output3, X, 4, 0, Z), " &
"7 (BC_1, D(3), input, X), " &
"8 (BC_2, D(3), output3, X, 4, 0, Z), " &
"9 (BC_1, D(4), input, X), " &
"10 (BC_2, D(4), output3, X, 13, 0, Z), " &
"11 (BC_1, D(5), input, X), " &
"12 (BC_2, D(5), output3, X, 13, 0, Z), " &
"13 (BC_2, *, control, 0), " & -- d[7:4]
"14 (BC_1, D(6), input, X), " &
"15 (BC_2, D(6), output3, X, 13, 0, Z), " &
"16 (BC_1, D(7), input, X), " &
"17 (BC_2, D(7), output3, X, 13, 0, Z), " &
"18 (BC_1, D(8), input, X), " &
"19 (BC_2, D(8), output3, X, 22, 0, Z), " &
--num cell port function safe ccell dsval rslt
"20 (BC_1, D(9), input, X), " &
"21 (BC_2, D(9), output3, X, 22, 0, Z), " &
"22 (BC_2, *, control, 0), " & -- d[11:8]
"23 (BC_1, D(10), input, X), " &
"24 (BC_2, D(10), output3, X, 22, 0, Z), " &
"25 (BC_1, D(11), input, X), " &
"26 (BC_2, D(11), output3, X, 22, 0, Z), " &
"27 (BC_1, D(12), input, X), " &
"28 (BC_2, D(12), output3, X, 31, 0, Z), " &
"29 (BC_1, D(13), input, X), " &
"30 (BC_2, D(13), output3, X, 31, 0, Z), " &
"31 (BC_2, *, control, 0), " & -- d[15:12]
"32 (BC_1, D(14), input, X), " &
"33 (BC_2, D(14), output3, X, 31, 0, Z), " &
"34 (BC_1, D(15), input, X), " &
"35 (BC_2, D(15), output3, X, 31, 0, Z), " &
"36 (BC_1, D(16), input, X), " &
"37 (BC_2, D(16), output3, X, 40, 0, Z), " &
"38 (BC_1, D(17), input, X), " &
"39 (BC_2, D(17), output3, X, 40, 0, Z), " &
--num cell port function safe ccell dsval rslt
"40 (BC_2, *, control, 0), " & -- d[19:16]
"41 (BC_1, D(18), input, X), " &
"42 (BC_2, D(18), output3, X, 40, 0, Z), " &
"43 (BC_1, D(19), input, X), " &
"44 (BC_2, D(19), output3, X, 40, 0, Z), " &
"45 (BC_1, D(20), input, X), " &
"46 (BC_2, D(20), output3, X, 49, 0, Z), " &
"47 (BC_1, D(21), input, X), " &
"48 (BC_2, D(21), output3, X, 49, 0, Z), " &
"49 (BC_2, *, control, 0), " & -- d[23:20]
"50 (BC_1, D(22), input, X), " &
"51 (BC_2, D(22), output3, X, 49, 0, Z), " &
"52 (BC_1, D(23), input, X), " &
"53 (BC_2, D(23), output3, X, 49, 0, Z), " &
"54 (BC_1, D(24), input, X), " &
"55 (BC_2, D(24), output3, X, 58, 0, Z), " &
"56 (BC_1, D(25), input, X), " &
"57 (BC_2, D(25), output3, X, 58, 0, Z), " &
"58 (BC_2, *, control, 0), " & -- d[27:24]
"59 (BC_1, D(26), input, X), " &
--num cell port function safe ccell dsval rslt
"60 (BC_2, D(26), output3, X, 58, 0, Z), " &
"61 (BC_1, D(27), input, X), " &
"62 (BC_2, D(27), output3, X, 58, 0, Z), " &
"63 (BC_1, D(28), input, X), " &
"64 (BC_2, D(28), output3, X, 67, 0, Z), " &
"65 (BC_1, D(29), input, X), " &
"66 (BC_2, D(29), output3, X, 67, 0, Z), " &
"67 (BC_2, *, control, 0), " & -- d[31:28]
"68 (BC_1, D(30), input, X), " &
"69 (BC_2, D(30), output3, X, 67, 0, Z), " &
"70 (BC_1, D(31), input, X), " &
"71 (BC_2, D(31), output3, X, 67, 0, Z), " &
"72 (BC_1, A(9), input, X), " &
"73 (BC_2, A(9), output3, X, 76, 0, Z), " &
"74 (BC_1, A(8), input, X), " &
"75 (BC_2, A(8), output3, X, 76, 0, Z), " &
"76 (BC_2, *, control, 0), " & -- a[9:6]
"77 (BC_1, A(7), input, X), " &
"78 (BC_2, A(7), output3, X, 76, 0, Z), " &
"79 (BC_1, A(6), input, X), " &
--num cell port function safe ccell dsval rslt
"80 (BC_2, A(6), output3, X, 76, 0, Z), " &
"81 (BC_2, *, control, 0), " & -- a[5:4]
"82 (BC_1, A(5), input, X), " &
"83 (BC_2, A(5), output3, X, 81, 0, Z), " &
"84 (BC_1, A(4), input, X), " &
"85 (BC_2, A(4), output3, X, 81, 0, Z), " &
"86 (BC_2, *, control, 0), " & -- a[3:2]
"87 (BC_1, A(3), input, X), " &
"88 (BC_2, A(3), output3, X, 86, 0, Z), " &
"89 (BC_1, A(2), input, X), " &
"90 (BC_2, A(2), output3, X, 86, 0, Z), " &
"91 (BC_1, CLA, input, X), " &
"92 (BC_2, *, control, 0), " & -- a[1:0]
"93 (BC_1, A(1), input, X), " &
"94 (BC_2, A(1), output3, X, 92, 0, Z), " &
"95 (BC_1, A(0), input, X), " &
"96 (BC_2, A(0), output3, X, 92, 0, Z), " &
"97 (BC_2, TM(2), output3, X, 99, 0, Z), " &
"98 (BC_2, TM(1), output3, X, 99, 0, Z), " &
"99 (BC_2, *, control, 0), " & -- tln(1),tm[2:0]
--num cell port function safe ccell dsval rslt
"100 (BC_2, TM(0), output3, X, 99, 0, Z), " &
"101 (BC_2, TLN(1), output3, X, 99, 0, Z), " &
"102 (BC_2, R_W, output3, X, 104, 0, Z), " &
"103 (BC_2, SIZ(1), output3, X, 104, 0, Z), " &
"104 (BC_2, *, control, 0), " & -- tln(0),siz[1:0]
"105 (BC_2, SIZ(0), output3, X, 104, 0, Z), " &
"106 (BC_2, TLN(0), output3, X, 104, 0, Z), " &
"107 (BC_2, LOCKE, output3, X, 109, 0, Z), " &
"108 (BC_2, LOCK, output3, X, 109, 0, Z), " &
"109 (BC_2, *, control, 0), " & -- lock,locke
"110 (BC_2, BR, output3, X, 127, 0, Z), " &
"111 (BC_1, BB, input, X), " &
"112 (BC_2, *, control, 0), " & -- bb
"113 (BC_2, BB, output3, X, 112, 0, Z), " &
"114 (BC_1, SNOOP, input, X), " &
"115 (BC_2, *, control, 0), " & -- tip
"116 (BC_2, TIP, output3, X, 115, 0, Z), " &
"117 (BC_1, TS, input, X), " &
"118 (BC_2, *, control, 0), " & -- ts
"119 (BC_2, TS, output3, X, 118, 0, Z), " &
--num cell port function safe ccell dsval rslt
"120 (BC_1, BTT, input, X), " &
"121 (BC_2, *, control, 0), " & -- btt
"122 (BC_2, BTT, output3, X, 121, 0, Z), " &
"123 (BC_2, *, control, 0), " & -- sas
"124 (BC_2, SAS, output3, X, 123, 0, Z), " &
"125 (BC_2, PST(4), output3, X, 127, 0, Z), " &
"126 (BC_2, PST(3), output3, X, 127, 0, Z), " &
"127 (BC_2, *, control, 0), " & -- pst[4:0],br
"128 (BC_2, PST(2), output3, X, 127, 0, Z), " &
"129 (BC_2, PST(1), output3, X, 127, 0, Z), " &
"130 (BC_2, PST(0), output3, X, 127, 0, Z), " &
"131 (BC_1, TA, input, X), " &
"132 (BC_1, TEA, input, X), " &
"133 (BC_1, TRA, input, X), " &
"134 (BC_1, BG, input, X), " &
"135 (BC_1, BGR, input, X), " &
"136 (BC_1, TBI, input, X), " &
"137 (BC_1, AVEC, input, X), " &
"138 (BC_1, TCI, input, X), " &
"139 (BC_2, *, internal, X), " &
--num cell port function safe ccell dsval rslt
"140 (BC_4, CLK, input, X), " &
"141 (BC_1, CLKEN, input, X), " &
"142 (BC_1, IPL(0), input, X), " &
"143 (BC_1, IPL(1), input, X), " &
"144 (BC_1, IPL(2), input, X), " &
"145 (BC_1, RSTI, input, X), " &
"146 (BC_1, CDIS, input, X), " &
"147 (BC_1, MDIS, input, X), " &
"148 (BC_2, BS(3), output3, X, 150, 0, Z), " &
"149 (BC_2, BS(2), output3, X, 150, 0, Z), " &
"150 (BC_2, *, control, 0), " & -- bs[3:0]
"151 (BC_2, BS(1), output3, X, 150, 0, Z), " &
"152 (BC_2, BS(0), output3, X, 150, 0, Z), " &
"153 (BC_2, *, control, 0), " & -- ipend,rsto
"154 (BC_2, RSTO, output3, X, 153, 0, Z), " &
"155 (BC_2, IPEND, output3, X, 153, 0, Z), " &
"156 (BC_2, CIOUT, output3, X, 157, 0, Z), " &
"157 (BC_2, *, control, 0), " & -- upa[1:0],ciout
"158 (BC_2, UPA(0), output3, X, 157, 0, Z), " &
"159 (BC_2, UPA(1), output3, X, 157, 0, Z), " &
--num cell port function safe ccell dsval rslt
"160 (BC_2, *, internal, X), " &
"161 (BC_2, TT0, output3, X, 164, 0, Z), " &
"162 (BC_1, TT1, input, X), " &
"163 (BC_2, TT1, output3, X, 164, 0, Z), " &
"164 (BC_2, *, control, 0), " & -- a[11:10],TT[1:0]
"165 (BC_1, A(10), input, X), " &
"166 (BC_2, A(10), output3, X, 164, 0, Z), " &
"167 (BC_1, A(11), input, X), " &
"168 (BC_2, A(11), output3, X, 164, 0, Z), " &
"169 (BC_1, A(12), input, X), " &
"170 (BC_2, A(12), output3, X, 173, 0, Z), " &
"171 (BC_1, A(13), input, X), " &
"172 (BC_2, A(13), output3, X, 173, 0, Z), " &
"173 (BC_2, *, control, 0), " & -- a[15:12]
"174 (BC_1, A(14), input, X), " &
"175 (BC_2, A(14), output3, X, 173, 0, Z), " &
"176 (BC_1, A(15), input, X), " &
"177 (BC_2, A(15), output3, X, 173, 0, Z), " &
"178 (BC_1, A(16), input, X), " &
"179 (BC_2, A(16), output3, X, 182, 0, Z), " &
--num cell port function safe ccell dsval rslt
"180 (BC_1, A(17), input, X), " &
"181 (BC_2, A(17), output3, X, 182, 0, Z), " &
"182 (BC_2, *, control, 0), " & -- a[19:16]
"183 (BC_1, A(18), input, X), " &
"184 (BC_2, A(18), output3, X, 182, 0, Z), " &
"185 (BC_1, A(19), input, X), " &
"186 (BC_2, A(19), output3, X, 182, 0, Z), " &
"187 (BC_1, A(20), input, X), " &
"188 (BC_2, A(20), output3, X, 191, 0, Z), " &
"189 (BC_1, A(21), input, X), " &
"190 (BC_2, A(21), output3, X, 191, 0, Z), " &
"191 (BC_2, *, control, 0), " &
"192 (BC_1, A(22), input, X), " & -- a[23:20]
"193 (BC_2, A(22), output3, X, 191, 0, Z), " &
"194 (BC_1, A(23), input, X), " &
"195 (BC_2, A(23), output3, X, 191, 0, Z), " &
"196 (BC_1, A(24), input, X), " &
"197 (BC_2, A(24), output3, X, 200, 0, Z), " &
"198 (BC_1, A(25), input, X), " &
"199 (BC_2, A(25), output3, X, 200, 0, Z), " &
--num cell port function safe ccell dsval rslt
"200 (BC_2, *, control, 0), " & -- a[27:24]
"201 (BC_1, A(26), input, X), " &
"202 (BC_2, A(26), output3, X, 200, 0, Z), " &
"203 (BC_1, A(27), input, X), " &
"204 (BC_2, A(27), output3, X, 200, 0, Z), " &
"205 (BC_1, A(28), input, X), " &
"206 (BC_2, A(28), output3, X, 209, 0, Z), " &
"207 (BC_1, A(29), input, X), " &
"208 (BC_2, A(29), output3, X, 209, 0, Z), " &
"209 (BC_2, *, control, 0), " & -- a[31:28]
"210 (BC_1, A(30), input, X), " &
"211 (BC_2, A(30), output3, X, 209, 0, Z), " &
"212 (BC_1, A(31), input, X), " &
"213 (BC_2, A(31), output3, X, 209, 0, Z) ";
end MC68060;