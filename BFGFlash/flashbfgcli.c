/*
  FlashBFGCLI
  (C) 2022 Henryk Richter

  Flash kickstart modules to the BFG9060 from CLI

  Synopsis:
   FlashBFGCLI NOBOOTROM=NB/S KickModules/M

  Just pass the intended Kickstart modules to the flash utility
  in any order.

  TODO:
   - some more sanity checks on BootROM contents
   - append mode (scan from end of ROM for available
     space)
   - detect duplicates
      - scan resident tags
   - save/load flashrom image

   (- disable Multitasking while erasing/flashing)
*/
#include "hunkload.h"
#include "flash.h"
#include "reset.h"
#include "lists.h"
#include "mmustuff.h"
#include <dos/dos.h>
#include <exec/memory.h>
#include <exec/execbase.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/mmu.h>

#define TEMPLATE "NOBOOTROM=NB/S/K,KickModules/M"
struct myopts {
	ULONG   nobootrom;
	char    **modnamearray;
};

#ifndef AFF_68060
#define AFF_68060 (1<<7)
#endif

struct Library *MMUBase;

extern unsigned char  bootrom_bin[];
extern unsigned long  bootrom_size;

void print_flashtype( struct flashrw *fsh);
STRPTR flash_errmsg( long errcode );

int main( int argc, char **argv )
{
  unsigned char *writebuffer = NULL;
  unsigned char *curwb;
  struct ExecBase *SysBase = *(struct ExecBase**)0x4;
  struct RDArgs *rdargs;
  struct myopts opts;
  struct hunkload *hl;
  struct nodelist modlist; 
  long errcode = HLERR_OK;
  unsigned long bytesToFlash = 0;
  struct flashrw flashhandle;
  int needreset = 0; /* we need a reset (and DISABLE) when there are active modules in flash */

  opts.nobootrom = 0;
  opts.modnamearray = NULL;

  if (!(rdargs = ReadArgs( (STRPTR)TEMPLATE, (LONG*)&opts, NULL)))
  {
	Printf( (STRPTR)"Syntax: %s " TEMPLATE "\n",(ULONG)argv[0]);
	return 10;
  }

  if( !(SysBase->AttnFlags & (AFF_68040|AFF_68060) ))
  {
	Printf( (STRPTR)"This program requires a 68040 or 68060 CPU. Exiting.\n");
	return 5;
  }

  MMUBase = OpenLibrary( (STRPTR)"mmu.library", 46 );

  /* Printf( (STRPTR)"bootrom size is %lx pointer is %lx\n",bootrom_size,(unsigned long)bootrom_bin); */

  NEWLIST( &modlist ); 
  /* load kickstart modules */
  if( opts.modnamearray )
  {
	char **patharr  = (char **)opts.modnamearray;

	while( (*patharr) && (errcode == HLERR_OK) )
	{
		/* Printf((STRPTR)"Load %s\n",(ULONG)(*patharr) ); */
		hl = hunk_load( *patharr, 0, &errcode );
		if( !hl )
		{
			Printf((STRPTR)"Error loading %s\n",(ULONG)(*patharr) );
		}
		else
		{
			ADDTAIL( &modlist, &hl->n );
		}
		patharr++;
	}
  }

  /* init flash, get size and check writable flags */
  if( errcode == HLERR_OK )
  {
  	/* make flash writable, if necessary */
	setupMMU( (void *)FLASH_DEFAULT, FLASH_TOTALSIZE, 0 );

	errcode = flashrw_init( &flashhandle );
	Printf( (STRPTR)"Found flash device:\n");
	print_flashtype(&flashhandle);

#if 0
	/* debug: ignore flash detection result */
	errcode = HLERR_OK;
	flashhandle.frw_Base = 0xF00000;
	flashhandle.frw_TotalBytes = 64*1024*2;
#else
	if( errcode != FRWERR_OK )
	{
		Printf( (STRPTR)"Flash init failed, error code %ld = %s\n",errcode,(ULONG)flash_errmsg(errcode));
		if(errcode == -2){
			Printf( (STRPTR)"Please check your mmu-configuration:\nMake sure 0x00F00000 is writable (not \"ROM\")!\n");
		}
		errcode = HLERR_WRONGARG;
	}
	else
	{
		Printf( (STRPTR)" with %ld Bytes capacity detected.\n",(LONG)flashhandle.frw_TotalBytes);

		/* TODO: scan flash, if it's empty or just the bootrom, we will
		   _not_ require a reboot */

		/* scan resident modules list: if something is active between
		   (0xF0-0xF8)<<16) we will require a reboot
		*/
		{
		 APTR *sysmodlist = SysBase->ResModules;
		 while( *sysmodlist != NULL )
		 {
			if( (*sysmodlist >= (APTR)0xF00000) && (*sysmodlist < (APTR)0xF80000) )
				needreset = 1;
			sysmodlist++;
		 }
		}
	}
#endif
  }

  /* gather sizes of loaded files */
  if( (!opts.nobootrom) && (errcode == HLERR_OK) ) /* BootROM enabled? */
  {
	bytesToFlash = bootrom_size;
	Printf( (STRPTR)"BootROM is %ld bytes\n",bytesToFlash);
  }
  
  if( (errcode == HLERR_OK) && (opts.modnamearray ) )
  {
	char **patharr  = (char **)opts.modnamearray;

	hl = (struct hunkload*)GetHead( &modlist );
	while( hl != NULL )
	{
		bytesToFlash += hl->totalsize;
		Printf( (STRPTR)"%s is %ld bytes\n",(ULONG)*patharr,(ULONG)hl->totalsize);

		/* check if there is BSS (and complain) */
		{
			struct hunk *h;
			h = GetHead( &hl->hunks );
			while( h )
			{
				if( h->type == 0x3EB )
				{
					errcode = HLERR_UNIMPLEMENTED;
					Printf( (STRPTR)"Error: BSS Hunks cannot be embedded in Flash, module %s\n",(ULONG)*patharr);
					break;
				}

				h = GetSucc( h );
			}
			if( errcode != HLERR_OK )
				break;
		}

		hl = GetSucc( &hl->n );
		patharr++;
	}

	Printf( (STRPTR)"total Size %ld\n",bytesToFlash);
  }

  if( bytesToFlash > flashhandle.frw_TotalBytes ) /* implies errcode == HLERR_OK */
  {
      Printf( (STRPTR)"Need %ld bytes but Flash size is %ld bytes, aborting.\n",bytesToFlash,flashhandle.frw_TotalBytes);
      bytesToFlash = 0;
  }

  /* allocate write buffer */
  if( (bytesToFlash) && (errcode==HLERR_OK) )
  {
  	writebuffer = (unsigned char*)AllocVec( bytesToFlash, MEMF_CLEAR );
  }

  if( writebuffer )
  {
	unsigned char *destptr = (unsigned char*)flashhandle.frw_Base; /* final destination after flashing */

  	curwb = writebuffer;
	if( !opts.nobootrom )
	{
		CopyMem( bootrom_bin, curwb, bootrom_size );
		curwb += bootrom_size;
		destptr+=bootrom_size;
	}

	hl = (struct hunkload*)GetHead( &modlist );
	while( hl != NULL )
	{
		errcode = hunk_relocate( hl, destptr, curwb, 0 );
		if( errcode != HLERR_OK )
			break;
		destptr+=hl->totalsize;
		curwb += hl->totalsize;
		hl = GetSucc( &hl->n );
	}
  }

  if( (errcode == HLERR_OK) && (writebuffer != NULL) )
  {
      const char *str = "YES";
      char buf[4];
      int res,i;

      Printf( (STRPTR)"final ROM image is %ld bytes. Please confirm to proceed.\n",bytesToFlash);
      if( needreset )
	      Printf( (STRPTR)"A reboot is required after flashing.\n");
      Printf( (STRPTR)"Please type YES (in capital letters), followed by Return: ");
      Flush( Output() );
      Read( Input(), buf, 3 );

      for( res=1,i=0 ; i < 3 ; i++ )
      {
      	if( buf[i] != str[i] )
		res=0;
      }

      if( !res )
       Printf( (STRPTR)"Did not get YES, aborting.\n");
      else
      {
       Printf( (STRPTR)"Starting to erase/flash. Allow 5-10 seconds.\n");
       Flush( Output() );
       Delay(20);

       if( needreset )
       {
        Printf( (STRPTR)"Automatic reboot after flashing.\n");
        Flush( Output() );
	SuperState();
        Disable();
       }
       if( FRWERR_OK == flashrw_erase( &flashhandle, 0, flashhandle.frw_TotalBytes ) )
       {
		errcode = flashrw_write( &flashhandle, 0, bytesToFlash, writebuffer );
       }
       else
       {
       		if( !needreset )
			Printf( (STRPTR)"Error erasing flash.\n");
		else
		{
			KBDR_Blink();
		}
       }

	if( needreset )
	{
		/* cold reboot */
		KBDReset(); /* simulate CTRL-A-A, if that fails, jump into reset instruction of kickstart */
	}
	else
	{
		Flush( Input() );
		Flush( Output() );
	}
      }
  }

  /* ERROR or CANCEL, free allocated resources */
  if( writebuffer )
  {
	FreeVec( writebuffer );
  }

  /* unload kickstart modules */
  hl = (struct hunkload*)GetHead( &modlist );
  while( hl != NULL )
  {
  	REMOVE( &hl->n );
	hunk_free( hl );
	hl = (struct hunkload*)GetHead( &modlist );
  }

  FreeArgs( rdargs );
  CloseLibrary( MMUBase );

  Printf( (STRPTR)"Done.\n");

  return 0;
}

void print_flashtype( struct flashrw *fsh)
{

 switch( fsh->frw_Manufacturer )
 {
	case 0x1: Printf( (STRPTR)"AM");break; /* also: TI, Spansion, FT */
	case 0x20: Printf( (STRPTR)"ST");break;
	case 0xC2: Printf( (STRPTR)"MX");break;
	case 0xBF: Printf( (STRPTR)"SST" );break;
	default: Printf( (STRPTR)"unkown manufactor: 0x%x\n",fsh->frw_Manufacturer);break;
 }

 switch( fsh->frw_Model )
 {
	case 0x20: Printf( (STRPTR)"29F010");break;
	case 0xA4: Printf( (STRPTR)"29F040");break;
	case 0xB5: Printf( (STRPTR)"39SF010" );break;
	case 0xB6: Printf( (STRPTR)"39SF020" );break;
	case 0xB7: Printf( (STRPTR)"39SF040" );break;
	default: Printf( (STRPTR)"unknown model: 0x%x\n",fsh->frw_Model);break;
 }

}

STRPTR ErrStrings[8] = {
 (STRPTR)"OK", /* no problem */
 (STRPTR)"Parameter problem",
 (STRPTR)"Unknown flash",
 (STRPTR)"Sector write protection",
 (STRPTR)"Erase overflow",
 (STRPTR)"Write error",
 (STRPTR)"Unspecified error",
 (STRPTR)"$VER: FlashBFGCLI 0.5 (26.09.22) by Henryk Richter&Matthias Heinrichs"
};

STRPTR flash_errmsg( long errcode )
{
	if( (errcode < -6) || (errcode > 0) )
		errcode = -6;

	return ErrStrings[-errcode];
}

