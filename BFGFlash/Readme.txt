FlashBFGCLI
(C) 2022 Henryk Richter

Introduction
------------

 This is a program to write Kickstart modules and MC68060 support code
 into the FlashROM of the BFG9060 by Matthias Heinrichs.

 The special MC68060 support code (aka BootROM) is required for the 
 Kickstart versions 3.0,3.1,3.5 and 3.9 to make them tolerate the precence
 of a full MC68060 processor. Technically, it switches off the FPU while
 initializing the operating system. Later Kickstart versions 3.1.4 and 3.2
 have this procedure built-in such that the BootROM is no longer strictly
 necessary. Also, the MC68LC060 and MC68EC060 don't feature an FPU and
 will work without BootROM.


Requirements
------------

 - Amiga 3000/4000 with BFG9060 CPU card
 - Kickstart 3.1.4, 3.2 or later (*1)
 

Invocation
----------

 FlashBFGCLI NOBOOTROM=NB/S,KickModules/M 

 NOBOOTROM   - Leave out the BootROM and just write Kickstart modules
 NB            to Flash (e.g. with Kickstart 3.1.4,3.2 ROMs).

 KickModules - List of Kickstart modules to write into Flash, separated
               by Space.

 In order to just write a valid BootROM into Flash, just call this tool
 from CLI without further parameters.

 Should the tool be able to correctly identify the Flash on the card,
 it's capacity sufficient for the modules and all modules to be of correct
 type, then the user is asked to confirm the flashing process.
 This query is to be answered with YES (in capital letters), followed
 by Return/Enter.

 In case that there were active Modules in the flash already, the program
 will disable Multitasking while writing the flash and reboot the Amiga
 automatically afterwards.

 Examples:

  Put Workbench.library and icon.library for 3.1.4 or 3.2 into the Flash:

   FlashBFGCLI LIBS:workbench.library LIBS:icon.library


Advantages and Caveats of the FlashROM on BFG9060
-------------------------------------------------

 The BootROM of 68060 cards for Amiga computers is located in a memory
 region that will be scanned by all Kickstart version at Cold- and Warm-
 boots. Hence, Kickstart components can be put into this area as alternatives
 or updates to those in the ROM chips on the Mainboard, which will
 be picked up automatically.

 The continued development of AmigaOS to 3.5/3.9 and later branches 3.1.4
 and 3.2 included possibilities to enhance existing ROM functionality by
 software or RAM based replacements to the respective modules. This way
 older ROMs like 3.1 could get improvements and fixes to the original
 ROM modules by loading the replacements from harddisk. While this
 approach is highly flexible, it's downside is an additional reboot 
 at an initial startup of the Amiga.

 A Flash can offer a middle ground between physical updates of Kickstart
 ROMs (by swapping chips) and software based loading of modules from HDD.

 Especially Expansion.library, which currently cannot be updated by 
 software based methods works just fine out of the Flash of the BFG9060.


MMU Configuration hints
-----------------------

 The tool writes to the address-space 0x00F00000-0x00F7FFFF. Some mmu-
 configurations declare this region "ROM" and prohibit writing to this space.
 Therefore, the common mmu.library requires the following line in the file 
 "ENVARC:MMU-configuration" with FlashBFGClI 0.1 and 0.2:

 SetCacheMode from 0x00F00000 size 0x00080000 Valid CACHEINHIBIT NOROM NOTWRITEPROTECTED

 Version 0.3 of FlashBFGCLI and later will adjust the MMU settings 
 automatically using mmu.library and won't need the line above. In this 
 case, the following line is more appropriate for good performance:

 SetCacheMode from 0x00F00000 size 0x00080000 Valid CopyBack NOROM NOTWRITEPROTECTED


Notes
-----

(*1): In order to make BFG9060 with Kickstart 3.0 through 3.9 working
      with a full MC68060, a pre-programmed Flash is absolutely necessary.
      Also, should the BootROM contents get deleted on those Kickstarts,
      the Amiga will not be able to boot. That's why a current Kickstart
      version is recommended.
      This recommendation does not apply to MC68LC060 and MC68040 which
      will work out of the box with any Kickstart (from 2.0 on).


Version History
---------------

0.4 - added SST39SF0x0 flash type support
0.3 - added MMU library support for automatic configuration of the
      relevant memory space
0.2 - first published version, supports 29F0x0
