/*
  load Hunk executables and store stuff
  (including reloc) in internal structures

  (C) 2022 Henryk Richter
*/

#include "hunkload.h"
#include "fileio.h"

#if 0
#define FH_T FILE*
#define FILE_OPEN_R( a ) fopen( (const char *)(a), "rb" )
#define FILE_OPEN_W( a ) fopen( (const char *)(a), "wb" )
#define FILE_CLOSE( fh ) fclose( fh )
#define FILE_READ( dest, nbytes, fh ) fread( (void *)(dest), 1, nbytes, fh )
#define FILE_WRITE( src, nbytes, fh ) fwrite( (void*)(src), 1, nbytes, fh )
#define FILE_SEEK_START( pos, fh ) fseek( fh, pos, SEEK_SET )
#define FILE_SEEK_CUR( pos, fh )   fseek( fh, pos, SEEK_CUR )
#define FILE_SEEK_END( pos, fh )   fseek( fh, pos, SEEK_END )
#endif

long hunk_free_attribs( struct hunk *h );
long hunk_loadrelocs( struct hunkload *hunkfile, struct hunk *h, FH_T infile, long type, long length );
long hunk_loadshortrelocs( struct hunkload *hunkfile, struct hunk *h, FH_T infile, long type, long length );
long hunk_loadcodedata( struct hunkload *hunkfile, struct hunk *h, FH_T fh, long type, long length );
long Get_CLong( FH_T fh );
long hunk_relocate_single( struct hunkload *hunkfile, struct hunk *h, void *base_address, void *save_address, long fileoffset );

#define HLERRBRK( _ecode_ ) { errstate = _ecode_; break; }

/* load hunk executable file, parse and store relevant data*/
struct hunkload *hunk_load( char *path, unsigned long flags, long *errcode )
{
 struct hunkload *ret = NULL;
 FH_T   infile;
 long   errstate = HLERR_OK;
 long   t,c;

 do
 {
 	infile = FILE_OPEN_R( path );
 	if( !infile )
		HLERRBRK( HLERR_FILEERR );

	ret = (struct hunkload*)CALLOC( sizeof( struct hunkload ) );
	if( !ret )
		HLERRBRK( HLERR_MEMORY );

	NEWLIST( &ret->hunks );

	/* check header */
	t = Get_CLong(infile);
	if( t != 0x3F3 )
		HLERRBRK( HLERR_NOTEXE );

	/* resident libraries are ignored here (irrelevant for practical use) */
	do
	{
		/* skip N1 name tag */
		t = Get_CLong(infile);
		if( t < 0 )
			HLERRBRK( HLERR_WRONGARG );
		if( t > 0 )
			FILE_SEEK_CUR( (t<<2), infile );
	}
	while( t > 0 );
	if( t < 0 )
		break;	/* bail out if an error occured above */

	/* number of hunks, first and last hunk */
	ret->nhunks    = Get_CLong(infile);
	ret->firsthunk = Get_CLong(infile);
	ret->lasthunk  = Get_CLong(infile);
	t = (ret->nhunks | ret->firsthunk | ret->lasthunk);
	/* error check: any number <0, hunk index problem */
	if( (t & (1<<30)) || (ret->firsthunk > ret->lasthunk) )
		HLERRBRK( HLERR_WRONGARG );

	D(("Nhunks %ld, First Hunk %ld, Last Hunk %ld\n",ret->nhunks,ret->firsthunk,ret->lasthunk));

	/* next: 
	    table with hunk sizes
	    - we preallocate hunk and associated data structures 
	      directly while traversing the table
	    - with that, we know to have enough memory for the hunks
	      themselves (but not necessarily for the relocs)
	  note: the separate allocation of the node and the actual memory is useful
	        for a LoadSeg replacement, where the internal nodes might be purged
		and the memory for the segments is kept
	  note(2): an application that just loads the code/data/bss to create ROM
	           images or compresses the content doesn't require specific RAM
	*/
	c = ret->lasthunk - ret->firsthunk + 1;
	while( c-- )
	{
		struct hunk *h;
		long memflags;

		h = (struct hunk*)CALLOC( sizeof( struct hunk ) );
		if( !h )
		{
			D(("Out of memory CALLOC\n"));
			HLERRBRK( HLERR_MEMORY );
		}
		NEWLIST( &h->attribs );
		ADDTAIL( &ret->hunks, &(h->n) );

		t = Get_CLong(infile);
		if( t == 0xFFFFFFFF )
		{
			D(("cannot load more hunk info got %lx\n",t));
			HLERRBRK( HLERR_WRONGARG );
		}
		memflags = (t>>29)&0x7; /* extract memory type(s) */
		t &= 0x0fffffff;

		if( memflags == 0x6 ) /* MEMF_CHIP|MEMF_FAST ? */
		{
			memflags = Get_CLong(infile);
			D(("MEMF_CHIP|MEMF_FAST present, loading additional word for memory type\n"));
		}


		ret->totalsize += (t<<2);
		h->size    = (t<<2); /* leave out the extra +4 below */
		h->memtype = memflags;
		/* note: while a hunk's size may be zero, the LOAD_OFFSET 
		         will ensure to allocate something meaningful */
#ifndef LOADSEG_COMPAT
		h->p = (unsigned char*)MALLOC( ((t<<2)+LOAD_OFFSET) ); /* +4 if the loaded hunk is rewritten to a segment list */
#else
		h->p = (unsigned char*)MALLOC_TYPE( ((t<<2)+LOAD_OFFSET), memflags ); /* +4 if the loaded hunk is rewritten to a segment list */
#endif
		D(("Hunk size %ld memory flags 0x%lx address %lx hunkdata %lx\n",(long)(t<<2),memflags,(long)h,(long)h->p));

		if( !(h->p) )
		{
			D(("Out of memory\n"));
			HLERRBRK( HLERR_MEMORY );
		}
	}
	if( errstate != HLERR_OK )
		break;

	/*
	   now load each hunk and also store relocs, if present
	*/
	{
	 struct hunk *h = GetHead( &ret->hunks ); /* current hunk */
	 long l,switched;
	 c = 0; /* this is now the next hunk index, also total number of loaded hunks  */
	 switched = 1;
	 do
	 {
		t = Get_CLong(infile); /* hunk type */
		/* D(("t is %lx\n",t)); */
		if( t < 0 )
			HLERRBRK( HLERR_WRONGARG );
		if( errstate != HLERR_OK )
			break;
		if( t == 0x3F2 ) /* HUNK_END */
		{
			c++;	/* current total */
			h = GetSucc( &h->n ); /* next hunk */
			switched = 1;

			D(("Hunk_end (count %ld cmp %ld)\n",c,(ret->lasthunk - ret->firsthunk + 1)));
			if( c < (ret->lasthunk - ret->firsthunk + 1) ) /* no point to continue if we have all code/data/bss hunks */
			{
				continue;
			}
			break;
		}
		l = Get_CLong(infile); /* hunk length in LWs */
		if( l < 0 )
			HLERRBRK( HLERR_WRONGARG );
		switch( t )
		{
			/* we do not support overlays. Period. */
			case 0x3F5:
				HLERRBRK( HLERR_OVERLAY );
				break;
			/* external symbols in an executable file ? -> nope. */
			case 0x3EF:
				HLERRBRK( HLERR_WRONGARG );
				break;
			case 0x3E9:	/* HUNK_CODE */
			case 0x3EA:	/* HUNK_DATA */
				if( !switched ) /* this may be over-engineered: if code/data hunks appear without prior HUNK_END, switch to next hunk here, otherwise rely on HUNK_END check above */
				{
					c++; h = GetSucc( &h->n );
				}
				switched = 0;
				D(("CODE/DATA Hunk %lx length %ld\n",t,(l<<2)));
				errstate = hunk_loadcodedata( ret, h, infile, t, l );
				break;
			case 0x3EB:	/* HUNK_BSS */
				D(("BSS Hunk %lx length %ld\n",t,(l<<2)));
				if( !switched )
				{
					c++; h = GetSucc( &h->n );
				}
				switched = 0;
				break;
			/* these were added with Kickstart 2.0 and 3.0 */
			/* HUNK_RELOC32SHORT is currently untested, as well as RELRELOC32 */
			case 0x3FD:	/* HUNK_RELRELOC32 */
			case 0x3FC:	/* HUNK_RELOC32SHORT */
			case 0x3F7:	/* HUNK_DREL32 */
				D(("DREL32/RELOC32SHORT/RELRELOC32 %lx\n",t));
				/* loop across hunks is within routine */
				errstate = hunk_loadshortrelocs( ret, h, infile, t, l ); 
				break;
			case 0x3ED: /* reloc16 */
			case 0x3EE: /* reloc8 (?) */
				D(("Reloc16, Reloc8 are not supposed to be in Executables\n"));
				errstate = HLERR_UNIMPLEMENTED;
				break;
			case 0x3EC: /* reloc32 */
				/* loop until we get 0 relocs */
				while( (l > 0) && (errstate==HLERR_OK) )
				{
					errstate = hunk_loadrelocs( ret, h, infile, t, l ); 
					l = Get_CLong(infile); /* hunk length in LWs */
				}
				break;
			case 0x3F0: /* symbols */
				/* loop until we get 0 length */
				while( l > 0 )
				{
					FILE_SEEK_CUR( (l<<2)+4, infile );
					/* D(("Skip Symbols %ld\n",(l<<2) )); */
					l = Get_CLong(infile); /* hunk length in LWs */
				}
				break;
			case 0x3F1: /* HUNK_DEBUG -> could be merged with default, just here for DBG output*/
				D(("HUNK_DEBUG length %ld\n",(l<<2)));
				FILE_SEEK_CUR( (l<<2), infile );
				break;
			default: 
				/* not interested: skip hunk in source */
				D(("Skipping Hunk %lx length %ld\n",t,(l<<2)));
				FILE_SEEK_CUR( (l<<2), infile );
				break;
		}
	 }
	 while( c <= (ret->lasthunk - ret->firsthunk + 1) );
	}
	if( errstate != HLERR_OK )
		break;

	/* file is parsed */

//HLERR_OK,HLERR_FILEERR,HLERR_MEMORY,HLERR_NOTEXE,HLERR_UNKRELOC,HLERR_OVERLAY
 } while(0);

 if( infile )
 	FILE_CLOSE( infile );

 if( errstate )
 {
 	hunk_free( ret );
	ret = NULL;
 }

 /* update retrurn value, if given */
 if( errcode )
 	*errcode = errstate;

 return ret;
}

/*
  Get a longword from input handle
  
  This function assumes that relevant longwords are positive numbers,
  hence if the MSB of the returned long is set, this condition is 
  used as an error indicator. Please don't use this function for
  regular code/data/reloc reading.
*/
long Get_CLong( FH_T fh )
{
	long res,ret;

	res = FILE_READ( &ret, 4, fh );
	if( res != 4 )
	{
		//D(("wanted 4, got %ld\n",res));
		ret = ~0;
	}
	else
	{
		//D(("ret is %lx\n",ret));
#ifndef USE_AMIGADOS
		/* possibly little endian, collect big endian bytes */
		long t;
		unsigned char *b = (unsigned char*)&ret;
		t = (((long)b[0])<<24) + (((long)b[1])<<16) + (((long)b[2])<<8) + (long)b[3];
		ret = t;
#endif
	}

	return ret;
}

/* load code/data hunk contents 

   length is supposed to be in longwords (not bytes!)
*/
long hunk_loadcodedata( struct hunkload *hunkfile, struct hunk *h, FH_T infile, long type, long length )
{
	unsigned char *p;

	if( !(hunkfile) || !(h) )
		return HLERR_WRONGARG;
	if( !h->p )
		return HLERR_MEMORY;
	if( (length<<2) > h->size ) /* actual size > than table size ? */
		return HLERR_WRONGARG;

	h->type = type; /* keep types: HUNK_CODE,HUNK_DATA */
	h->size = (length<<2);
	h->flags= 0;

	p = h->p + LOAD_OFFSET;

	D(("Reading %ld bytes from file\n",h->size));

	if( h->size != FILE_READ( p, h->size, infile ) )
		return HLERR_FILEERR;

	D(("Code/Data read\n"));

	return HLERR_OK;
}

long hunk_loadrelocs( struct hunkload *hunkfile, struct hunk *h, FH_T infile, long type, long length )
{
	struct attribs *a;
	long *p;
	long l;

	a = MALLOC( sizeof( struct attribs ) + length*sizeof(long) );
	if( !a )
		return HLERR_MEMORY;

	D(("Load Relocs %ld bytes\n",(length<<2)));

	l = Get_CLong(infile); /* destination hunk */
	if( (l < 0) || (l > hunkfile->lasthunk) )
	{
		MFREE(a);
		return HLERR_WRONGARG;
	}

	switch( type )
	{
		case	0x3EC: /* reloc32 */
			a->type = A_RELOC32;
			break;
		case	0x3ED: /* reloc16 */
			a->type = A_RELOC16;
			break;
		case	0x3EE:
		default:
			MFREE( a );
			return HLERR_WRONGARG;
			break;
	}

	a->size = length;
	a->dest = l;
	p = (long*)(a+1);

	//D(("a is %lx p is %lx sizeof(a) is %lx hunk is %lx\n",(long)a,(long)p,(long)sizeof(struct attribs),(long)h));
#if 1
	while( length-- )
	{
		l = Get_CLong(infile); /* destination hunk */
		if( l < 0 )
		{
			MFREE(a);
			D(("Cannot read Relocs\n"));
			return HLERR_WRONGARG;
		}
		/* D(("r %ld\n",l)); */
		*p++ = l;
	}
#else
	/* works only for big endian systems, we need relocs natively */ 
	if( (length<<2) != FILE_READ( p, (length<<2), infile ) )
	{
		MFREE(a);
		D(("Cannot read Relocs %ld\n",(length<<2) ));
		return HLERR_FILEERR;
	}
#endif
	//D(("Loaded relocs\n"));

	ADDTAIL( &h->attribs, &a->n );

	//D(("Adding Reloc Attrib %lx dest %ld entries %ld\n",type,a->dest,length));

	return HLERR_OK;
}

/*
  put new word in upper 16 bits
*/
long hunk_getw( FH_T infile, long curword, long *nwords )
{
	long l;

	if( *nwords == 0 )
	{
		l = Get_CLong(infile);
		if( l == 0xFFFFFFFFL )
		{
			*nwords = -1;
			return 0;
		}
		*nwords = 1;
	}
	else
	{
		*nwords = *nwords-1;
		l=(curword&0xffff)<<16;
	}

	//D(("l %lx nw %ld\n",l,*nwords));
	return l;
}

/*
 load short relocs and convert them to regular 32 bit relocs
*/
long hunk_loadshortrelocs( struct hunkload *hunkfile, struct hunk *h, FH_T infile, long type, long destlength )
{
	struct attribs *a;
	long *p;
	long length,nw,cw;

	nw = 1;			/* current number of words in buffer */
	cw = destlength;	/* current words (0-2) */

	/* we come here with (nrelocs<<16)|desthunk */
	while( cw >= 0x10000 ) /* if( nrelocs<<16 != 0 ) */
	{
		length = (cw>>16)&0xffff;
		cw = hunk_getw( infile, cw, &nw ); /* dest hunk */
		if( (nw < 0) || ((cw>>16)>hunkfile->lasthunk) )
			return HLERR_FILEERR;

		a = MALLOC( sizeof( struct attribs ) + length*sizeof(long) );
		if( !a )
			return HLERR_MEMORY;

		a->size = length; /* nrelocs   */
		a->dest = cw>>16; /* dest hunk */
		p = (long*)(a+1);

		switch( type )
		{
			case 0x3FD:	/* HUNK_RELRELOC32 (PC relative) */
				a->type = A_RELRELOC32;
				break;
			case 0x3FC:	/* HUNK_RELOC32SHORT */
			case 0x3F7:	/* HUNK_DREL32 */
				a->type = A_RELOC32;
				break;
			default:
				MFREE( a );
				return HLERR_WRONGARG;
				break;
		}

		ADDTAIL( &h->attribs, &a->n );

		while( length-- )
		{
			cw   = hunk_getw( infile, cw, &nw ); /* pos */
			if( nw < 0 )
				return HLERR_FILEERR;

			*p++ = (cw>>16)&0xffff;
			/* D(("offset %ld\n",(cw>>16)&0xffff)); */
		}
		cw = hunk_getw( infile, cw, &nw ); /* next length */
	}

	return HLERR_OK;
}


/* compress code and data of hunk executable */
long hunk_pack( struct hunkload *hunkfile )
{
	return 0;
}

long hunk_unpack( struct hunkload *hunkfile )
{
	return 0;
}

/* offset wrt. base address by summing up hunk sizes */
long hunk_offset( struct hunkload *hunkfile, long index )
{
	long off = 0;
	struct hunk *h = GetHead( &hunkfile->hunks );

	while( index-- ) 
	{
		off += h->size;
		h = GetSucc( &h->n );
	}
	return off;
}

#ifdef USE_AMIGADOS
#define R32( _loc_, _off_ ) \
{ long *_dp_ = (long*)(_loc_);\
  *_dp_ += (long)(_off_);\
}
#else
#define R32( _loc_, _off_ ) \
 { long content = ((long)_loc_[0]<<24) +\
                  ((long)_loc_[1]<<16) +\
		  ((long)_loc_[2]<<8)  +\
		  (long)_loc_[3];\
   content += (long)(_off_);\
   _loc_[0] = (content>>24)&0xff;\
   _loc_[1] = (content>>16)&0xff;\
   _loc_[2] = (content>>8)&0xff;\
   _loc_[3] = content&0xff;\
 }
#endif

long hunk_relocate_single( struct hunkload *hunkfile, struct hunk *h, void *base_address, void *save_address, long fileoffset )
{
	struct attribs *a;
	long hunkoffset;
	int  i;
	long *p;
	unsigned char *sp;

	a = GetHead( &h->attribs );
	while( (a) )
	{
		p = (long*)(a+1);
		hunkoffset = (long)base_address + hunk_offset( hunkfile, a->dest ); /* position to relocate to */

		if( a->type == A_RELOC32 )
		{
			for( i=0 ; i < a->size ; i++ )
			{
				/* location in memory plus location which is to be relocated */
				sp = (unsigned char*)save_address + fileoffset + *p++;
				R32( sp, hunkoffset );
			}
		}

		if( a->type == A_RELRELOC32 )
		{
			for( i=0 ; i < a->size ; i++ )
			{
				/* location in memory plus location which is to be relocated */
				sp = (unsigned char*)save_address + fileoffset + *p++;
				/* TODO: apply pc relative reloc */
//				DR32( sp, hunkoffset );
			}
		}

		a = GetSucc( &a->n );
	}

	return (long)h->size;
}

/*
  This code relocates the loaded file. Two operations are possible:
  - in-place (base_address==0,save_address==0)
     This operation may serve as LoadSeg replacement and relocates the
     current file in memory. Please note that LOADSEG_COMPAT must be compiled
     in to load the input file in proper memory types. Without LOADSEG_COMPAT,
     the memory type flags are not respected.
     Note: In-place relocation may only be done ONCE!
  - ROM mode (base_address!=0,save_address!=0)
     Code, Data and BSS are placed physically at save_address which must 
     be able to hold hunkfile->totalsize bytes and relocated to the native 
     address "base_address". Examples to typical base_address locations for 
     ExtROMs or DiagROMs are 0xE80000 or 0xF00000.
     Since data is copied from the hunkfile contents to the save_address,
     relocations of a single hunkfile to some remote location may be done
     repeatedly. hunkfile->totalsize is guaranteed to be divisible by 4.
     Limitations for ROM modules:
      - memory type flags are ignored
      - BSS sections are stored by this code - but are useless because ROM space
        is by definition read-only
      - all Data is CONST in ROM space

  inputs:
   hunkfile     - loaded hunkfile
   base_address - final start address where the code is supposed to be run from
   save_address - storage pointer for the contents of the hunks (consecutively), needs room
                  for hunkfile->totalsize bytes
   flags        - keep 0, for now
  output: HLERR Code
*/
long hunk_relocate( struct hunkload *hunkfile, void *base_address, void *save_address, unsigned long flags )
{
	struct hunk *h;
	unsigned char *p;
	long fileoffset;

	if( !hunkfile )
		return HLERR_WRONGARG;

	/* loadseg style in-place relocation is not implemented yet */
	if( !(base_address) || !(save_address) )
		return HLERR_UNIMPLEMENTED;

	/* empty ? */
	h = GetHead( &hunkfile->hunks );
	if( !h )
		return HLERR_WRONGARG;

	fileoffset = 0;
	while( h )
	{
		if( !(p=h->p) )
			return HLERR_WRONGARG;

		/* copy contents */
		MEMCPY( (unsigned char*)save_address + fileoffset, 
		        (unsigned char*)p + LOAD_OFFSET,
		        h->size ); 

		/* traverse relocation attributes */
		hunk_relocate_single( hunkfile, h, base_address, save_address, fileoffset );

		fileoffset += h->size;
		h = GetSucc( &h->n );
	}

	return HLERR_OK;
}


long hunk_free_attribs( struct hunk *h )
{
	struct attribs *a;

	if( h )
	{
		while( (a = GetHead( &h->attribs ) ))
		{
			REMOVE(a);
			MFREE(a);
		}
	}

	return HLERR_OK;
}

/* release file */
long hunk_free( struct hunkload *hunkfile )
{
	struct hunk *h;

	D(("hunk_free %lx\n",(unsigned long)hunkfile));

	if( !hunkfile )
		return HLERR_WRONGARG;

	/* traverse hunks and free associated data */
	while( (h = GetHead( &hunkfile->hunks )) )
	{
		REMOVE(h);
		hunk_free_attribs( h );
		if( h->p )
			MFREE( h->p );
		MFREE(h);
	}

	MFREE( hunkfile );
	return HLERR_OK;
}
