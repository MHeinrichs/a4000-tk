#ifndef _INLINE_MC68040_H
#define _INLINE_MC68040_H

#ifndef CLIB_MC68040_PROTOS_H
#define CLIB_MC68040_PROTOS_H
#endif

#ifndef  EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef  LIBRARIES_68040_H
#include <libraries/68040.h>
#endif

#ifndef MC68040_BASE_NAME
#define MC68040_BASE_NAME MC68040Base
#endif

#define FPUControl(flags, mask) ({ \
  ULONG _FPUControl_flags = (flags); \
  ULONG _FPUControl_mask = (mask); \
  ({ \
  register char * _FPUControl__bn __asm("a6") = (char *) (MC68040_BASE_NAME);\
  ((ULONG (*)(char * __asm("a6"), ULONG __asm("d0"), ULONG __asm("d1"))) \
  (_FPUControl__bn - 222))(_FPUControl__bn, _FPUControl_flags, _FPUControl_mask); \
});})

#endif /*  _INLINE_MC68040_H  */
