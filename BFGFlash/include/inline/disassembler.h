#ifndef _INLINE_DISASSEMBLER_H
#define _INLINE_DISASSEMBLER_H

#ifndef CLIB_DISASSEMBLER_PROTOS_H
#define CLIB_DISASSEMBLER_PROTOS_H
#endif

#ifndef  EXEC_TYPES_H
#include <exec/types.h>
#endif
#ifndef  LIBRARIES_DISASSEMBLER_H
#include <libraries/disassembler.h>
#endif

#ifndef DISASSEMBLER_BASE_NAME
#define DISASSEMBLER_BASE_NAME DisassemblerBase
#endif

#define Disassemble(data) ({ \
  struct DisData * _Disassemble_data = (data); \
  ({ \
  register char * _Disassemble__bn __asm("a6") = (char *) (DISASSEMBLER_BASE_NAME);\
  ((APTR (*)(char * __asm("a6"), struct DisData * __asm("a0"))) \
  (_Disassemble__bn - 66))(_Disassemble__bn, _Disassemble_data); \
});})

#define FindStartPosition(startpc, min, max) ({ \
  APTR _FindStartPosition_startpc = (startpc); \
  UWORD _FindStartPosition_min = (min); \
  UWORD _FindStartPosition_max = (max); \
  ({ \
  register char * _FindStartPosition__bn __asm("a6") = (char *) (DISASSEMBLER_BASE_NAME);\
  ((APTR (*)(char * __asm("a6"), APTR __asm("a0"), UWORD __asm("d0"), UWORD __asm("d1"))) \
  (_FindStartPosition__bn - 72))(_FindStartPosition__bn, _FindStartPosition_startpc, _FindStartPosition_min, _FindStartPosition_max); \
});})

#endif /*  _INLINE_DISASSEMBLER_H  */
